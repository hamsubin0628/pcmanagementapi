package com.example.pcmanagementapi.entity;

import com.example.pcmanagementapi.enums.PcManagementEnum;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Sort;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class PcManagement {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private Short seatName;

    @Column(nullable = false)
    private LocalDate dateCreate;

    @Column(nullable = false, length = 10)
    @Enumerated(value = EnumType.STRING)
    private PcManagementEnum pcCheck;

    @Column(columnDefinition = "TEXT")
    private String checkMemo;

    @Column(nullable = false)
    private Boolean upgrade;
}
