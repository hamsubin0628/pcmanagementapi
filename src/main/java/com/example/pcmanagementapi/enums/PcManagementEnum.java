package com.example.pcmanagementapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PcManagementEnum {
    NORMAL ("정상", false),
    FAULTY ("불량", true);

    private final String status;
    private final Boolean change;
}
