package com.example.pcmanagementapi.enums;

import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class PcManagementItem {
    private Short seatName;
    private LocalDate dateCreate;
    private String pcManagementEnum;
    private String checkMemo;
    private Boolean upgrade;
}
