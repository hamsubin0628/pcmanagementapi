package com.example.pcmanagementapi.repository;

import com.example.pcmanagementapi.entity.PcManagement;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PcManagementRepository extends JpaRepository<PcManagement, Long> {
}
