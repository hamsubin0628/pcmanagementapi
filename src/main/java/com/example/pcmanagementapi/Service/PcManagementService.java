package com.example.pcmanagementapi.Service;

import com.example.pcmanagementapi.entity.PcManagement;
import com.example.pcmanagementapi.enums.PcManagementEnum;
import com.example.pcmanagementapi.enums.PcManagementItem;
import com.example.pcmanagementapi.model.PcManagementRequest;
import com.example.pcmanagementapi.repository.PcManagementRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PcManagementService {
    private final PcManagementRepository pcManagementRepository;

    public void setPcManagement(PcManagementRequest request){
        PcManagement addData = new PcManagement();
        addData.setSeatName(request.getSeatName());
        addData.setDateCreate(LocalDate.now());
        addData.setPcCheck(request.getPcCheck());
        addData.setCheckMemo(request.getCheckMemo());
        addData.setUpgrade(request.getUpgrade());

        pcManagementRepository.save(addData);
    }

    public List<PcManagementItem> getPcManagements(){
        List<PcManagement> originList = pcManagementRepository.findAll();

        List<PcManagementItem> result = new LinkedList<>();

        for (PcManagement pcManagement:originList) {
            PcManagementItem addItem = new PcManagementItem();
            addItem.setSeatName(pcManagement.getSeatName());
            addItem.setDateCreate(pcManagement.getDateCreate());
            addItem.setPcManagementEnum(pcManagement.getPcCheck().getStatus());
            addItem.setCheckMemo(pcManagement.getCheckMemo());
            addItem.setUpgrade(pcManagement.getUpgrade());

            result.add(addItem);
        }

        return result;
    }
}
