package com.example.pcmanagementapi.model;

import com.example.pcmanagementapi.enums.PcManagementEnum;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Sort;

import java.time.LocalDate;

@Getter
@Setter
public class PcManagementRequest {
    private Short seatName;

    private LocalDate dateCreate;

    @Enumerated(value = EnumType.STRING)
    private PcManagementEnum pcCheck;

    private String checkMemo;

    private Boolean upgrade;
}
