package com.example.pcmanagementapi.Controller;

import com.example.pcmanagementapi.Service.PcManagementService;
import com.example.pcmanagementapi.entity.PcManagement;
import com.example.pcmanagementapi.enums.PcManagementItem;
import com.example.pcmanagementapi.model.PcManagementRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/management")
public class PcManagementController {
    private final PcManagementService pcManagementService;

    @PostMapping("/pc")
    public String setPcManagement(@RequestBody PcManagementRequest request){
        pcManagementService.setPcManagement(request);

        return "OK";
    }

    @GetMapping("/all")
    public List<PcManagementItem> getPcManagement() {
        return pcManagementService.getPcManagements();
    }
}
